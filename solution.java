import java.util.*;
//Time complexity = O(N)
//Space complexity = O(N)
class Solution {
    public int findJudge(int n, int[][] trust) {
        int len= trust.length;
        if(n==1 && len==0)return 1;
        
        Hashtable<Integer, Integer> listTrusted = new Hashtable<>();
        HashSet<Integer> listTruster = new HashSet<>();
        
        //O(N)
        for(int i =0; i<len ;i++){
            int trusted=trust[i][1];
            int truster=trust[i][0];
            
            //creating a map cointaining the trested people and the number of people how trust him
            if(!listTrusted.containsKey(trusted)) listTrusted.put(trusted,1);
            //creating a set containing the people how trust other
            else listTrusted.put(trusted,listTrusted.get(trusted)+1);
            
            listTruster.add(truster);
        }
        
        //O(N)
        for (Map.Entry<Integer, Integer> e : listTrusted.entrySet()){
            //the judge is trusted by everybody except himself (n-1) && he trust nobody
            if((e.getValue() == n-1) && (!listTruster.contains(e.getKey()))){
                return e.getKey();
            }
        }

        return -1;
    }
}
